using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chino.Core.EFCore.Sqlite
{
    public static class DatabaseExtensions
    {
        public static void RegisterSqliteChinoAppDbContext<TDbContext>(this IServiceCollection services, IConfiguration configuration) 
            where TDbContext : DbContext
        {
            string connectionString = configuration.GetConnectionString("Chino_App_Sqlite");
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<TDbContext>(options =>
                options.UseSqlite(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly)));
        }
    }
}
