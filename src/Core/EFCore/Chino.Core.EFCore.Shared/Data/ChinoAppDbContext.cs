using Chino.Core.Shared.Models.Role;
using Chino.Core.Shared.Models.User;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Chino.Core.EFCore.Shared.Data
{
    public class ChinoAppDbContext : IdentityDbContext<ChinoUser, ChinoRole, string>
    {
        public ChinoAppDbContext(DbContextOptions<ChinoAppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
