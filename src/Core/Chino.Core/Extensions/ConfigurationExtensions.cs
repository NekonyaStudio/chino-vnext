using Chino.Core.Shared.Const;
using Microsoft.Extensions.Configuration;

namespace Chino.Core
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// 是否使用汉语输出Log
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static bool CmnLog(this IConfiguration configuration)
            => configuration.GetValue<bool>(ConfKeyConst.UseCmnHansLog);
    }
}
