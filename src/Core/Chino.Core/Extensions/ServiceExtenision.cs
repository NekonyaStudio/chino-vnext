using System;
using Chino.EFCore.Mysql;
using Chino.EFCore.PostgreSQL;
using Chino.EFCore.Sqlite;
using Chino.EFCore.SqlServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nekonya;

namespace Chino.Core
{
    public static class ServiceExtenision
    {
        public static void AddChinoCoreServices(this IServiceCollection services)
        {
            //services.AddScoped<ISignInService, SignInService>();
        }


        public static void AddChinoDatabase<TDbContext>(this IServiceCollection services, IConfiguration configuration) 
            where TDbContext : DbContext
        {
            string providerType = configuration["Database:ProviderType:Chino"] ?? "sqlite";
            string providerStr = configuration["OverrideDbProvider"];

            if (!providerStr.IsNullOrEmpty()) //覆盖数据库提供者设定，这个是给添加数据库迁移的powershell用的。
                providerType = providerStr;


            switch (providerType.ToLower())
            {
                case "mysql":
                case "mariadb":
                    services.RegisterMysqlChinoDbContext<TDbContext>(ref configuration);
                    break;

                case "sqlite":
                case "sqlite3":
                    services.RegisterSqliteChinoDbContext<TDbContext>(ref configuration);
                    break;

                case "sqlserver":
                case "mssql":
                    services.RegisterSqlServerChinoDbContext<TDbContext>(ref configuration);
                    break;

                case "postgresql":
                    services.RegisterPostgreSQLChinoDbContext<TDbContext>(ref configuration);
                    break;

                default:
                    throw new Exception($"Unknow database provider type: {providerType}");
            }
        }
    }
}
