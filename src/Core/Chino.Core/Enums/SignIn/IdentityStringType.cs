namespace Chino.Core.Enums.SignIn
{
    /// <summary>
    /// 身份字符串的类型
    /// </summary>
    public enum IdentityStringType : int
    {
        Unknow          = 0,
        UserName        = 1,
        PhoneNumber     = 2,
        EmailAddress    = 3,
    }
}
