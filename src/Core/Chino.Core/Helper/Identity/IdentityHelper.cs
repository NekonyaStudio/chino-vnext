using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Chino.Core.Helper.Identity
{
    public static class IdentityHelper
    {
        /// <summary>
        /// 应用 Chino IdentityOptions 强制规则
        /// </summary>
        public static void ApplyChinoIdentityOptionsMandatoryRules(IdentityOptions options)
        {
            options.User.AllowedUserNameCharacters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._+"; //用户名允许的字母，比默认规则少了个@, 以免和邮箱搞混了
        }
    }
}
