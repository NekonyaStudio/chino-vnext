using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chino.Core.Shared.Const
{
    public static class ChinoConst
    {
        /// <summary>
        /// 可以访问管理员面板的策略名
        /// </summary>
        public const string AdminDashboardPolicyName = @"admin_dashboard";
    }
}
