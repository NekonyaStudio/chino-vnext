namespace Chino.Core.Shared.Const
{
    /// <summary>
    /// Chino 内置角色名
    /// </summary>
    public static class ChinoRoleConst
    {
        /// <summary>
        /// 可以访问Chino后台的权限
        /// </summary>
        public const string Admin = @"Chino_Admin";

        /// <summary>
        /// Chino系统最高权限
        /// </summary>
        public const string Root = @"Chino_Root";
    }
}
