namespace Chino.Core.Shared.Const
{
    /// <summary>
    /// 配置的Key
    /// </summary>
    public static class ConfKeyConst
    {
        /// <summary>
        /// 使用汉语输出Log
        /// </summary>
        public const string UseCmnHansLog = "Chino:CmnHansLog";
    }
}
