using Microsoft.AspNetCore.Identity;

namespace Chino.Core.Shared.Models.Role
{
    public class ChinoRole : IdentityRole
    {
        public ChinoRole() { }
        public ChinoRole(string roleName) : base(roleName) { }
    }
}
