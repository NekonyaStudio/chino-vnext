using Microsoft.AspNetCore.Identity;

namespace Chino.Core.Shared.Models.User
{
    public class ChinoUser : IdentityUser
    {
        /// <summary>
        /// 手机号的国家代码，【不包含+号】
        /// </summary>
        public string PhoneCountryCode { get; set; }
    }
}
