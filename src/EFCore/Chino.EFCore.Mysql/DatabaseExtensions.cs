using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chino.EFCore.Mysql
{
    public static class DatabaseExtensions
    {
        public static void RegisterMysqlChinoDbContext<TDbContext>(this IServiceCollection services, ref IConfiguration configuration)
            where TDbContext : DbContext
        {
            string connectionString = configuration.GetConnectionString("Chino_Mysql");
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<TDbContext>(options =>
                options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString), sql => sql.MigrationsAssembly(migrationsAssembly)));
        }
    }
}
