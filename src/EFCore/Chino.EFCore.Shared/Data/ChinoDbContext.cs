using Chino.Core.Shared.Models.Role;
using Chino.Core.Shared.Models.User;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Chino.EFCore.Shared.Data
{
    public class ChinoDbContext : IdentityDbContext<ChinoUser, ChinoRole, string>
    {
        public ChinoDbContext(DbContextOptions<ChinoDbContext> options) : base(options)
        {
        }
    }
}
