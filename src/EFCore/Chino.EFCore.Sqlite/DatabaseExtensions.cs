using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chino.EFCore.Sqlite
{
    public static class DatabaseExtensions
    {
        public static void RegisterSqliteChinoDbContext<TDbContext>(this IServiceCollection services, ref IConfiguration configuration)
            where TDbContext : DbContext
        {
            string connectionString = configuration.GetConnectionString("Chino_Sqlite");
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<TDbContext>(options =>
                options.UseSqlite(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly)));
        }
    }
}
