using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chino.EFCore.SqlServer
{
    public static class DatabaseExtensions
    {
        public static void RegisterSqlServerChinoDbContext<TDbContext>(this IServiceCollection services, ref IConfiguration configuration)
            where TDbContext : DbContext
        {
            string connectionString = configuration.GetConnectionString("Chino_SqlServer");
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<TDbContext>(options =>
                options.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly)));
        }
    }
}
