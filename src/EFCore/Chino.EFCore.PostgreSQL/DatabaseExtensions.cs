using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chino.EFCore.PostgreSQL
{
    public static class DatabaseExtensions
    {
        public static void RegisterPostgreSQLChinoDbContext<TDbContext>(this IServiceCollection services, ref IConfiguration configuration)
            where TDbContext : DbContext
        {
            string connectionString = configuration.GetConnectionString("Chino_PostgreSQL");
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<TDbContext>(options =>
                options.UseNpgsql(connectionString, sql => sql.MigrationsAssembly(migrationsAssembly)));
        }
    }
}
