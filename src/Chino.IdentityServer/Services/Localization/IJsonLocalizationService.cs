using System.Globalization;

namespace Chino.IdentityServer.Services.Localization
{
    public interface IJsonLocalizationService
    {
        string this[string key, CultureInfo cultureInfo] { get; }
        string this[string key, CultureInfo cultureInfo, params string[] args] { get; }

        /// <summary>
        /// 从 appsettings.json 中获取文本值，如果配置的值是 {{value}} 格式，则去json本地化字典中寻找"value"为key的内容，否则直接返回 appsettings.json 中取到的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cultureInfo"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        string GetConfigurationValue(string key, CultureInfo cultureInfo, string defaultValue = null);
        string GetText(string key, string culture, string defaultValue = null);
        string GetText(string key, CultureInfo cultureInfo, string defaultValue = null);


    }
}
