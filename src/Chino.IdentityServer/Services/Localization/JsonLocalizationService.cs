using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Nekonya;

namespace Chino.IdentityServer.Services.Localization
{
    public class JsonLocalizationService : IJsonLocalizationService
    {
        private readonly ILogger<JsonLocalizationService> m_Logger;
        private readonly IConfiguration m_Configuration;
        private IConfiguration m_JsonLocalization;

        public JsonLocalizationService(IHostEnvironment environment,
            ILogger<JsonLocalizationService> logger,
            IConfiguration configuration)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("Localization/localization.json")
                .AddJsonFile($"Localization/localization.{environment.EnvironmentName}.json", true, true);

            try
            {
                m_JsonLocalization = builder.Build();
            }
            catch(FileNotFoundException e)
            {
                m_Logger.LogWarning("Json localization service not ready: {0}", e.Message);
            }

            this.m_Logger = logger;
            this.m_Configuration = configuration;
        }

        public string this[string key, CultureInfo cultureInfo]
        {
            get
            {
                return GetText(key, cultureInfo, key);
            }
        }

        public string this[string key, CultureInfo cultureInfo, params string[] args]
        {
            get
            {
                return string.Format(GetText(key, cultureInfo, key), args);
            }
        }

        public string GetText(string key, string culture, string defaultValue = null)
        {
            if (key.IsNullOrEmpty())
                return defaultValue;
            string final_key = $"{key}:{culture}";
            string final_key_default = $"{key}:default";
            var value = m_JsonLocalization[final_key];
            if (value == null)
                return m_JsonLocalization.GetValue<string>(final_key_default, defaultValue);
            else
                return value;
        }

        public string GetText(string key, CultureInfo cultureInfo, string defaultValue = null)
        {
            if (key.IsNullOrEmpty())
                return defaultValue;
            //string final_key_default = $"{key}:default";

            if (cultureInfo == null)
                return m_JsonLocalization.GetValue<string>(GetDefaultKey(key), defaultValue);

            return GetTextWithCultureInfoRecursion(key, cultureInfo, null);
        }

        /// <summary>
        /// 从 appsettings.json 中获取文本值，如果配置的值是 {{value}} 格式，则去json本地化字典中寻找"value"为key的内容，否则直接返回 appsettings.json 中取到的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="cultureInfo"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string GetConfigurationValue(string key, CultureInfo cultureInfo, string defaultValue = null)
        {
            string value = m_Configuration[key];
            if (value == null)
                return defaultValue;
            if (value.StartsWith("{{") && value.EndsWith("}}"))
            {
                return GetText(value.Substring(2, value.Length - 4), cultureInfo, defaultValue);
            }
            else
                return value;
        }

        private string GetTextWithCultureInfoRecursion(string key, CultureInfo current_CultureInfo, List<CultureInfo> recursionChain = null)
        {
            if (recursionChain == null)
                recursionChain = new List<CultureInfo>();
            recursionChain.Add(current_CultureInfo);

            //string final_key_default = $"{key}:default";
            string final_key = $"{key}:{current_CultureInfo.Name}";
            //m_Logger.LogDebug("递归查找Key: {0}", final_key);
            string value = m_JsonLocalization[final_key];
            if (value == null)
            {
                if(current_CultureInfo.Parent != null)
                {
                    if(!recursionChain.Contains(current_CultureInfo.Parent))
                    {
                        return GetTextWithCultureInfoRecursion(key, current_CultureInfo.Parent, recursionChain);
                    }
                }

                return m_JsonLocalization.GetValue<string>(GetDefaultKey(key));
            }
            else
                return value;
        }

        private string GetDefaultKey(string pureKey)
            => $"{pureKey}:default";

    }
}
