using System.Globalization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;

namespace Chino.IdentityServer.Services.Localization
{
    public class RequestJsonLocalizationService : IRequestJsonLocalization
    {
        private readonly IJsonLocalizationService m_JsonLocalization;
        private readonly IHttpContextAccessor m_HttpContextAccessor;
        private readonly CultureInfo m_CurrentUICulture;

        public RequestJsonLocalizationService(IJsonLocalizationService jsonLocalization,
            IHttpContextAccessor httpContextAccessor)
        {
            this.m_JsonLocalization = jsonLocalization;
            this.m_HttpContextAccessor = httpContextAccessor;
            m_CurrentUICulture = httpContextAccessor.HttpContext.Features.Get<IRequestCultureFeature>()?.RequestCulture?.UICulture;
        }

        public string this[string key] => m_JsonLocalization[key, m_CurrentUICulture];

        public string GetConfigurationValue(string key, string defaultValue = null)
            => m_JsonLocalization.GetConfigurationValue(key, m_CurrentUICulture, defaultValue);

        public string GetText(string key, string defaultValue = null)
            => m_JsonLocalization.GetText(key, m_CurrentUICulture, defaultValue);
    }
}
