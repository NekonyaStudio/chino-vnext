namespace Chino.IdentityServer.Services.Localization
{
    /// <summary>
    /// 在某一个请求中的Json本地化
    /// 本接口是对IJsonLocalizationService的进一步封装，在每次请求中自动获取到用户的本地化区域信息
    /// </summary>
    public interface IRequestJsonLocalization
    {
        string this[string key] { get; }

        string GetText(string key, string defaultValue = null);

        /// <summary>
        /// 从 appsettings.json 中获取文本值，如果配置的值是 {{value}} 格式，则去json本地化字典中寻找"value"为key的内容，否则直接返回 appsettings.json 中取到的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        string GetConfigurationValue(string key, string defaultValue = null);
    }
}
