using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Chino.Core;
using Chino.Core.Helper.Identity;
using Chino.Core.Shared.Const;
using Chino.Core.Shared.Models.Role;
using Chino.Core.Shared.Models.User;
using Chino.EFCore.Shared.Data;
using Chino.IdentityServer.Resources.DataAnnotation;
using Chino.IdentityServer.Services.Localization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Chino.IdentityServer
{
    public class Startup
    {
        private readonly IConfiguration m_Configuration;
        private readonly IHostEnvironment m_Environment;

        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            this.m_Configuration = configuration;
            this.m_Environment = env;
        }

        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages(options =>
            {
                options.Conventions.AuthorizeFolder("/Admin", ChinoConst.AdminDashboardPolicyName);
            })
                .AddViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.SubFolder)
                //.AddChinoViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.SubFolder)
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                    {
                        var assemblyName = new AssemblyName(typeof(DataAnnotationResources).GetTypeInfo().Assembly.FullName);
                        return factory.Create($"DataAnnotation.{nameof(DataAnnotationResources)}", assemblyName.Name);
                    };
                });

            //数据库
            services.AddChinoDatabase<ChinoDbContext>(m_Configuration);

            //Identity
            services.AddIdentity<ChinoUser, ChinoRole>(options =>
            {
                //应用 Chino IdentityOptions 强制规则
                IdentityHelper.ApplyChinoIdentityOptionsMandatoryRules(options);
            })
                .AddEntityFrameworkStores<ChinoDbContext>()
                .AddDefaultTokenProviders();

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //})
            //    .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            //    {
            //        Serilog.Log.Logger.Information("returnUrl: {0}", options.ReturnUrlParameter);
            //    });

            services.AddAuthentication();
            services.AddAuthorization(options =>
            {
                options.AddPolicy(ChinoConst.AdminDashboardPolicyName, policy =>
                {
                    policy.RequireRole(ChinoRoleConst.Admin, ChinoRoleConst.Root);
                });
            });

            //本地化
            #region 本地化
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new List<CultureInfo>
                {
                    new CultureInfo("ja"),
                    new CultureInfo("en"),
                    new CultureInfo("zh"),
                    new CultureInfo("zh-CN"),
                    new CultureInfo("en-US"),
                    new CultureInfo("ja-JP"),

                };

                var supportedUICultures = new List<CultureInfo>
                {
                    //new CultureInfo("ja"),
                    new CultureInfo("en"),
                    new CultureInfo("zh"),
                    new CultureInfo("ja-JP"),
                };

                options.DefaultRequestCulture = new RequestCulture("zh");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedUICultures;
            });

            #endregion

            //Chino.Core里面注册的各种服务
            services.AddChinoCoreServices();

            //Chino Services
            services.AddSingleton<IJsonLocalizationService, JsonLocalizationService>();
            services.AddScoped<IRequestJsonLocalization, RequestJsonLocalizationService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRequestLocalization(options =>
            {
            });

            
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
            });
        }
    }
}
