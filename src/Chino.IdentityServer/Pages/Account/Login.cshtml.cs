using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Chino.Core.Enums.SignIn;
using Chino.Core.Shared.Models.User;
using Chino.EFCore.Shared.Data;
using Chino.IdentityServer.Services.Localization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Nekonya;

namespace Chino.IdentityServer.Pages.Account
{
    public class LoginModel : PageModel
    {
        private readonly ILogger<LoginModel> m_Logger;
        private readonly UserManager<ChinoUser> m_UserManager;
        private readonly SignInManager<ChinoUser> m_SignInManager;
        private readonly ChinoDbContext m_DbContext;
        private readonly IRequestJsonLocalization m_JsonLocalizer;
        private readonly IStringLocalizer<LoginModel> m_Localizer;

        public LoginModel(ILogger<LoginModel> logger,
            UserManager<ChinoUser> userManager,
            SignInManager<ChinoUser> signInManager,
            ChinoDbContext dbContext,
            IRequestJsonLocalization jsonLocalizer,
            IStringLocalizer<LoginModel> localizer)
        {
            this.m_Logger = logger;
            this.m_UserManager = userManager;
            this.m_SignInManager = signInManager;
            this.m_DbContext = dbContext;
            this.m_JsonLocalizer = jsonLocalizer;
            this.m_Localizer = localizer;
        }

        [BindProperty(SupportsGet = true)]
        public string ReturnUrl { get; set; }

        /// <summary>
        /// 身份字符串
        /// </summary>
        [BindProperty]
        [Required(ErrorMessage = "emailOrUserNameRequired")]
        public string IdentityString { get; set; }

        public string IdentityStringErrorMessage { get; set; }

        public void OnGet(string identityStr)
        {
            if (!identityStr.IsNullOrEmpty())
                IdentityString = identityStr;
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if(await m_DbContext.Users.AsNoTracking().AnyAsync(u => u.UserName.Equals(IdentityString)))
            {
                //检索到用户名
                return RedirectToPage("Signin/Password", new { ReturnUrl = this.ReturnUrl, IdentityString = this.IdentityString, IdentityType = (int)IdentityStringType.UserName });
            }


            ModelState.AddModelError(nameof(IdentityString), m_Localizer["account_notfound", m_JsonLocalizer.GetConfigurationValue("Chino:AccountName")]);
            return Page();
        }
    }
}
