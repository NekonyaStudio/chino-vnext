using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Chino.Core;
using Chino.Core.Enums.SignIn;
using Chino.Core.Shared.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Nekonya;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Chino.IdentityServer.Pages.Account.Signin
{
    public class PasswordModel : PageModel
    {
        private readonly SignInManager<ChinoUser> m_SignInManager;
        private readonly UserManager<ChinoUser> m_UserManager;
        private readonly IConfiguration m_Configuration;
        private readonly IStringLocalizer<PasswordModel> m_Localizer;
        private readonly ILogger<PasswordModel> m_Logger;

        public PasswordModel(SignInManager<ChinoUser> signInManager,
            UserManager<ChinoUser> userManager,
            IConfiguration configuration,
            IStringLocalizer<PasswordModel> localizer,
            ILogger<PasswordModel> logger)
        {
            this.m_SignInManager = signInManager;
            this.m_UserManager = userManager;
            this.m_Configuration = configuration;
            this.m_Localizer = localizer;
            this.m_Logger = logger;
        }


        [BindProperty(SupportsGet = true)]
        public string ReturnUrl { get; set; }


        /// <summary>
        /// 身份字符串
        /// </summary>
        [BindProperty(SupportsGet = true)]
        [Required(ErrorMessage = "emailOrUserNameRequired")]
        public string IdentityString { get; set; }

        /// <summary>
        /// 对应枚举类型
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public int IdentityType { get; set; }

        [Required]
        [BindProperty]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [BindProperty]
        public bool RememberLogin { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var type = (IdentityStringType)IdentityType;

            SignInResult result = null;
            ChinoUser user = null;

            if(type == IdentityStringType.UserName)
            {
                user = await m_UserManager.FindByNameAsync(IdentityString);
                if(user == null)
                {
                    ModelState.AddModelError(string.Empty, m_Localizer["invalid_username", IdentityString]);

                    #region Log
                    if (m_Configuration.CmnLog())
                        m_Logger.LogInformation("来自IP {0} 的登录请求被拒绝：未找到用户名 \"{1}\" 对应的用户信息", HttpContext.Connection.RemoteIpAddress, IdentityString);
                    else
                        m_Logger.LogInformation("Login request from IP {0} is denied: user is not found by username \"{1}\".", HttpContext.Connection.RemoteIpAddress, IdentityString);
                    #endregion

                    return Page();
                }
                result = await m_SignInManager.PasswordSignInAsync(user, Password, RememberLogin, true);
                if (result.Succeeded)
                {
                    #region Log
                    if (m_Configuration.CmnLog())
                        m_Logger.LogInformation("用户 {0}({1})[{2}] 登录成功，来源IP：{3}", user.UserName ?? "*没有用户名", user.Email ?? "*没有邮箱", user.Id, HttpContext.Connection.RemoteIpAddress);
                    else
                        m_Logger.LogInformation("User {0}({1})[{2}] logged in successfully, source IP: {3}", user.UserName ?? "*No UserName", user.Email ?? "*No Email", user.Id, HttpContext.Connection.RemoteIpAddress);
                    #endregion

                    if (Url.IsLocalUrl(ReturnUrl))
                        return Redirect(ReturnUrl);
                    else if (ReturnUrl.IsNullOrEmpty())
                        return Redirect("/");
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, m_Localizer["password_login_failed"]);

                    if (m_Configuration.CmnLog())
                        m_Logger.LogInformation("来自IP {0} 的登录请求被拒绝: 用户名 {1} 与密码不匹配", HttpContext.Connection.RemoteIpAddress, IdentityString);
                    else
                        m_Logger.LogInformation("Login request from IP {0} is denied: username {1} does not match password", HttpContext.Connection.RemoteIpAddress, IdentityString);

                    return Page();
                }
            }


            
            return Page();
        }
    }
}
