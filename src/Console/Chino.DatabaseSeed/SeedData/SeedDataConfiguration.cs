using System.Collections.Generic;

namespace Chino.DatabaseSeed.SeedData
{
    public class SeedDataConfiguration
    {
        public List<SeedUser> Users { get; set; }

        public List<string> Roles { get; set; }

        public class SeedUser
        {
            public string UserName { get; set; }
            public string Email { get; set; }
            public bool EmailConfirmed { get; set; } = true;
            public string Password { get; set; }

            public string Name { get; set; }
            public string NickName { get; set; }


            public bool Admin { get; set; } = false;
            public bool Root { get; set; } = false;
        }
    }
}
