using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Chino.Core;
using Chino.Core.Shared.Const;
using Chino.Core.Shared.Models.Role;
using Chino.Core.Shared.Models.User;
using Chino.EFCore.Shared.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nekonya;
using Serilog;

namespace Chino.DatabaseSeed.SeedData
{
    public static class SeedData
    {
        public static void EnsureSeedData(IConfiguration configuration)
        {
            SeedDataConfiguration seedDataConfig;
            try
            {
                string seedDataJson = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "seedData.json"), Encoding.UTF8);
                seedDataConfig = JsonSerializer.Deserialize<SeedDataConfiguration>(seedDataJson);
            }
            catch (Exception e)
            {
                Console.WriteLine("[Read SeedData From Json File Failed!] " + e.Message);
                throw;
            }

            var services = new ServiceCollection();
            services.AddLogging();
            services.AddChinoDatabase<ChinoDbContext>(configuration);
            services.AddIdentity<ChinoUser, ChinoRole>(options =>
            {

            })
                .AddEntityFrameworkStores<ChinoDbContext>()
                .AddDefaultTokenProviders(); //照理说好像不需要加这一句吧？

            using var serviceProvider = services.BuildServiceProvider();
            using var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

            
            var chinoAppDbContext = scope.ServiceProvider.GetService<ChinoDbContext>();
            var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ChinoUser>>();
            var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<ChinoRole>>();

            //数据库迁移
            chinoAppDbContext.Database.Migrate();

            //Seed 角色数据
            Log.Logger.Information("Ensure Roles");
            EnsureRoles(ref roleManager, ref seedDataConfig);

            //Seed 用户数据
            Log.Logger.Information("Ensure Users");
            EnsureUsers(ref userManager, ref roleManager, ref seedDataConfig, ref configuration);

        }

        private static void EnsureRoles(ref RoleManager<ChinoRole> roleManager, ref SeedDataConfiguration seedData)
        {
            var roleList = new List<string>()
            {
                ChinoRoleConst.Admin,
                ChinoRoleConst.Root,
            };

            if (seedData.Roles != null && seedData.Roles.Count > 0)
                roleList.AddRange(seedData.Roles);

            foreach (var roleName in roleList)
            {
                var role = roleManager.FindByNameAsync(roleName).Result;
                if (role == null)
                {
                    role = new ChinoRole(roleName);
                    Log.Logger.Information("[Role]Add role :{0}", roleName);
                    var result = roleManager.CreateAsync(role).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                }
            }
        }

        private static void EnsureUsers(ref UserManager<ChinoUser> userManager,
            ref RoleManager<ChinoRole> roleManager,
            ref SeedDataConfiguration seedData,
            ref IConfiguration configuration)
        {
            if(seedData.Users != null && seedData.Users.Count > 0)
            {
                foreach(var item in seedData.Users)
                {
                    var userInfo = userManager.FindByNameAsync(item.UserName).Result;
                    if(userInfo == null)
                    {
                        userInfo = new ChinoUser
                        {
                            UserName = item.UserName,
                            Email = item.Email
                        };
                        if (!userInfo.Email.IsNullOrEmpty())
                            userInfo.EmailConfirmed = item.EmailConfirmed;

                        Log.Logger.Information("[Users]Add user :{0} - {1}", item.UserName, item.Email ?? "(*No Email)");
                        var result = userManager.CreateAsync(userInfo, item.Password).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                    }

                    if(userInfo != null)
                    {
                        //检查内置Roles
                        if (item.Admin)
                        {
                            if (!userManager.IsInRoleAsync(userInfo, ChinoRoleConst.Admin).Result)
                                userManager.AddToRoleAsync(userInfo, ChinoRoleConst.Admin).Wait();
                        }
                        else
                        {
                            if (userManager.IsInRoleAsync(userInfo, ChinoRoleConst.Admin).Result)
                                userManager.RemoveFromRoleAsync(userInfo, ChinoRoleConst.Admin).Wait();
                        }

                        if (item.Root)
                        {
                            if (!userManager.IsInRoleAsync(userInfo, ChinoRoleConst.Root).Result)
                                userManager.AddToRoleAsync(userInfo, ChinoRoleConst.Root).Wait();
                        }
                        else
                        {
                            if (userManager.IsInRoleAsync(userInfo, ChinoRoleConst.Root).Result)
                                userManager.RemoveFromRoleAsync(userInfo, ChinoRoleConst.Root).Wait();
                        }
                    }

                    //Todo: Claims

                }
            }
        }
    }
}
